#!/bin/bash -l

source "$(dirname "$0")/common.sh"

Params = ""

if [[$APP_LOCATION != "" ]]
then
	Params += " --app "$APP_LOCATION
	
if [[$API_LOCATION != "" ]]
then
	Params += " --api "$API_LOCATION

if [[$OUTPUT_LOCATION != "" ]]
then
	Params += " --outputLocation "$OUTPUT_LOCATION

if [[$APP_BUILD_COMMAND != "" ]]
then
	Params += " --appBuildCommand "$APP_BUILD_COMMAND

if [[$API_BUILD_COMMAND != "" ]]
then
	Params += " --apiBuildCommand "$API_BUILD_COMMAND

if [[$ROUTES_LOCATION != "" ]]
then
	Params += " --routesLocation "$ROUTES_LOCATION

if [[$BUILD_TIMEOUT_IN_MIN != "" ]]
then
	Params += " --buildTimeoutInMinutes "$BUILD_TIMEOUT_IN_MIN

if [[$CONFIGFILE_LOCATION != "" ]]
then
	Params += " --configFileLocation "$CONFIGFILE_LOCATION
	
if [[ $ARTIFACT_LOCATION != "" ]]
then
    Params += " --appArtifactLocation "$ARTIFACT_LOCATION

if [[ $REPO_TOKEN != "" ]]
then
    Params += " --repoToken "$REPO_TOKEN

if [[$SKIP_APP_BUILD != "" ]]
then
	Params += " --skipAppBuild "$SKIP_APP_BUILD

if [[$IS_NEXT_APP != "" ]]
then
	Params += " --isNextApp "$IS_NEXT_APP

if [[ $VERBOSE != "" ]]
then
    Params += " --verbose "$VERBOSE


cd /bin/staticsites/
./StaticSitesClient --deploymentaction 'upload' --deploymentProvider 'Bitbucket' --apiToken $API_TOKEN "$Params"