# Bitbucket Pipelines Pipe: Azure Static Web Apps Deploy

Deploys an application to [Azure Static Web Apps](https://azure.microsoft.com/en-gb/services/app-service/static/). 

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
    - pipe: microsoft/azure-static-web-apps-deploy:1.0.1
      variables:
        APP_LOCATION: '$BITBUCKET_CLONE_DIR/<string>'
        API_TOKEN: '<string>'
        # API_LOCATION: '$BITBUCKET_CLONE_DIR/<string>' # Optional.
        # OUTPUT_LOCATION: '$BITBUCKET_CLONE_DIR/<string>'  # Optional.
```

## Variables

| Variable              | Usage                                                       |
| ------------------------ | ----------------------------------------------------------- |
| APP_LOCATION (*)         | The path to you application. |
| API_TOKEN (*)            | Deployment token (from Azure portal) of the Static site you created. |
| API_LOCATION             | The path to functions in your application(If there are any). |
| OUTPUT_LOCATION          | The path to the build output directory relative to the app_location. |

_(*) = required variable._


## Examples

### Basic example

```yaml
script:
  - pipe: microsoft/azure-static-web-apps-deploy:1.0.1
    variables:
      APP_LOCATION: '$BITBUCKET_CLONE_DIR/src'
      API_TOKEN: $deployment_token
```

### Advanced example

```yaml
script:
  - pipe: microsoft/azure-static-web-apps-deploy:1.0.1
    variables:
      APP_LOCATION: '$BITBUCKET_CLONE_DIR/src'
      API_LOCATION: '$BITBUCKET_CLONE_DIR/api'
      OUTPUT_LOCATION: '$BITBUCKET_CLONE_DIR'
      API_TOKEN: $deployment_token
```

## Support

This sample is provided "as is" and is not supported. Likewise, no commitments are made as to its longevity or maintenance. To discuss this sample with other users, please visit the Azure DevOps Services section of the Microsoft Developer Community: https://developercommunity.visualstudio.com/spaces/21/index.html.
